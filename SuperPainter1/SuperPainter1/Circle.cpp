#include "Circle.h"


Circle::Circle(const Point & center, double radius, const string & type, const string & name) : Shape(name, type)
{
	this->_center = center;
	this->_radius = radius;
}

Circle::~Circle()
{}

const Point & Circle::getCenter() const
{
	return this->_center;
}

double Circle::getRadius() const
{
	return this->_radius;
}

double Circle::getArea() const
{
	return PI * pow(getRadius(), POWER_OF_TWO);
}

double Circle::getPerimeter() const
{
	return MULTIPLY_BY_TWO * PI * getRadius();
}

void Circle::
move(const Point & other)
{
	this->_center = this->_center + other;
}

void Circle::printDetails() const
{
	cout << "%s", this->getName();
	cout << "  ";
	cout << "%s", this->getType();
	cout << "\t";
	cout << this->getArea();
	cout << "\t";
	cout << this->getPerimeter();
}

void Circle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLUE[] = { 0, 0, 255 };
	const Point& c = getCenter();
	board.draw_circle(c.getX(), c.getY(), getRadius(), BLUE, 100.0f).display(disp);	
}

void Circle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0 };
	const Point& c = getCenter();
}


