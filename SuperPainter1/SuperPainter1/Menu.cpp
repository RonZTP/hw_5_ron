#include "Menu.h"

Menu::Menu()
{
	_board = new cimg_library::CImg<unsigned char>(700, 700, 1, 3, 1);
	_disp = new cimg_library::CImgDisplay(*_board, "Super Paint");

	int mainInput = 0;

	cout << "Enter 0 to add a new shape." << endl;
	cout << "Enter 1 to modify or get information from a current shape." << endl;
	cout << "Enter 2 tp delete all of the shapes." << endl;
	cout << "Enter 3 to exit." << endl;

	switch (mainInput)
	{
	case 0:
	{
		int shapeChoice = 0;
		int x1 = 0;
		int y1 = 0;
		int x2 = 0;
		int y2 = 0;
		int x3 = 0;
		int y3 = 0;
		int radius = 0;
		int pointIndex = 1;
		string name;
		string type;
		Circle* circle = 0;

		cout << "Enter 0 to add a circle." << endl;
		cout << "Enter 1 to add an arrow" << endl;
		cout << "Enter 2 to add a triangle" << endl;
		cout << "Enter 3 to add a rectangle" << endl;
		cin >> shapeChoice;
		switch (shapeChoice)
		{
		case 0: //Circle
		{
			pointIndex = 1;
			type = "Circle";
			cout << "Please enter X of point number: " << pointIndex << endl;
			cin >> x1;
			cout << "Please enter Y of point number: " << pointIndex << endl;
			cin >> y1;
			cout << "Please enter radius:" << endl;
			cin >> radius;
			cout << "Please enter the name of the shape:" << endl;
			cin >> name;
			Point p1(x1, y1);
			*circle = Circle::Circle(p1, radius, type, name);

			break;
		}
		case 1: //Arrow
		{
			pointIndex = 1;
			type = "Arrow";
			cout << "Please enter X of point number: " << pointIndex << endl;
			cin >> x1;
			cout << "Please enter Y of point number: " << pointIndex << endl;
			cin >> y1;
			pointIndex++; //Increasing counter
			cout << "Please enter X of point number: " << pointIndex << endl;
			cin >> x2;
			cout << "Please enter Y of point number: " << pointIndex << endl;
			cin >> y2;
			cout << "Please enter the name of the shape:" << endl;
			cin >> name;
			Point p1(x1, y1);
			Point p2(x2, y2);
			Arrow::Arrow(p1, p2, type, name);
			break;
		}
		case 2: //Triangle
		{
			pointIndex = 1;
			type = "Triangle";
			cout << "Please enter X of point number: " << pointIndex << endl;
			cin >> x1;
			cout << "Please enter Y of point number: " << pointIndex << endl;
			cin >> y1;
			pointIndex++; //Increasing counter
			cout << "Please enter X of point number: " << pointIndex << endl;
			cin >> x2;
			cout << "Please enter Y of point number: " << pointIndex << endl;
			cin >> y2;
			pointIndex++; //Increasing counter
			cout << "Please enter X of point number: " << pointIndex << endl;
			cin >> x3;
			cout << "Please enter Y of point number: " << pointIndex << endl;
			cin >> y3;
			cout << "Please enter the name of the shape:" << endl;
			cin >> name;
			Point p1(x1, y1);
			Point p2(x2, y2);
			Point p3(x3, y3);
			Triangle::Triangle(p1, p2, p3, type, name);
			break;
		} //End of case 2
		} //End of switch case 0 - small
	} //End of case 0 - big
	} //End of switch case
}


Menu::~Menu()
{
	_disp->close();
	delete _board;
	delete _disp;
}
