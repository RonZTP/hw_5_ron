#include "Triangle.h"



Triangle::Triangle(const Point & a, const Point & b, const Point & c, const string & type, const string & name) : Polygon(name, type)
{
	bool flag = false;

	if (! ( (a.getX() == b.getX() && a.getX() == c.getX()) || (a.getY() == b.getY() && a.getY() == c.getY()) ) )
	{
		flag = true;
	}

	if (flag)
	{
		this->_points.push_back(a);
		this->_points.push_back(b);
		this->_points.push_back(c);
	}
	else
	{
		cout << "The points entered create a line." << endl;
	}
}

Triangle::~Triangle()
{}

double Triangle::getArea() const
{
	double ab = this->_points[POINT_A].distance(this->_points[POINT_B]); // a
 	double bc = this->_points[POINT_B].distance(this->_points[POINT_C]); // b
	double ca = this->_points[POINT_C].distance(this->_points[POINT_A]); // c
	
	//Calculating by Heron's law
	double elem1 = ab + bc + ca;
	double elem2 = ab + bc - ca;
	double elem3 = bc + ca - ab;
	double elem4 = ca + ab - bc;
	
	double overall_elem1 = elem1 * elem2 * elem3 * elem4;
	overall_elem1 = sqrt(overall_elem1);
	overall_elem1 = overall_elem1 / DIVISION_BY_FOUR;

	return overall_elem1;
}

double Triangle::getPerimeter() const
{
	double ab = this->_points[POINT_A].distance(this->_points[POINT_B]); // a
	double bc = this->_points[POINT_B].distance(this->_points[POINT_C]); // b
	double ca = this->_points[POINT_C].distance(this->_points[POINT_A]); // c

	return ab + bc + ca;
}

void Triangle::move(const Point & other)
{
	for (int i = 0; i < this->_points.size(); i++)
	{
		this->_points[i] = this->_points[i] + other;
	}
}

void Triangle::printDetails() const
{
	cout << "%s", this->getName();
	cout << "\t\t";
	cout << "%s", this->getType();
	cout << "\t";
	cout << this->getArea();
	cout << "       ";
	cout << this->getPerimeter();
}

void Triangle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char GREEN[] = { 0, 255, 0 };
	board.draw_triangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(),
		_points[2].getX(), _points[2].getY(), GREEN, 100.0f).display(disp);
}

void Triangle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0 };
	board.draw_triangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(),
		_points[2].getX(), _points[2].getY(), BLACK, 100.0f).display(disp);
}
