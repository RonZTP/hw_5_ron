#include "Point.h"
Point::Point() {}

Point::Point(double x, double y)
{
	this->_x = x;
	this->_y = y;
}

Point::Point(const Point & other)
{
	this->_y = other.getY();
	this->_x = other.getX();
}

Point::~Point()
{}

Point Point::operator+(const Point & other) const
{
	Point newPoint = Point((this->_x + other.getX()), (this->_y + other.getY()));
	return newPoint;
}

Point & Point::operator+=(const Point & other)
{
	this->_x = this->_x + other.getX();
	this->_y = this->_y + other.getY();
	return *this;
}

double Point::getX() const
{
	return this->_x;
}

double Point::getY() const
{
	return this->_y;
}

double Point::distance(const Point & other) const
{
	double x1 = this->_x;
	double x2 = other.getX();
	double y1 = this->_y;
	double y2 = other.getY();
	double distance = 0;
	double reminder_x = 0;
	double reminder_y = 0;

	reminder_x = x2 - x1;
	reminder_y = y2 - y1;

	reminder_x = pow(reminder_x, POWER_OF_TWO);
	reminder_y = pow(reminder_y, POWER_OF_TWO);

	distance = sqrt(reminder_x + reminder_y);
	
	return distance;
}
