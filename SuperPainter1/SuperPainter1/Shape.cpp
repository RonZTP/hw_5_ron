#include "Shape.h"

Shape::Shape()
{
	this->_name = "default name";
	this->_type = "default type";
}

Shape::Shape(const string & name, const string & type)
{
	this->_name = name;
	this->_type = type;
}

void Shape::printDetails() const
{
	cout << "%s", this->getName();
	cout << "/t";
	cout << "%s", this->getType();
	cout << "/t";
}

string Shape::getType() const
{
	return this->_type;
}

string Shape::getName() const
{
	return this->_name;
}
