#include "Rectangle.h"

myShapes::Rectangle::Rectangle(const Point & a, double length, double width, const string & type, const string & name) : Polygon (name, type)
{
	if (width > 0 && length > 0)
	{
		this->_points.push_back(a);

		int second_x = 0, second_y = 0;
		//Calculating the next point.
		second_x = a.getX() + width;
		second_y = a.getY() + length;
		Point b(second_x, second_y);
		this->_points.push_back(b);

		this->_length = length;
		this->_width = width;
	}
	else
	{
		cout << "Length or Width can't be 0." << endl;
	}
}

myShapes::Rectangle::~Rectangle()
{}

double myShapes::Rectangle::getArea() const
{ 
	double width = this->_width;
	double length = this->_length;
	double area = width * length;
	return area;
}

double myShapes::Rectangle::getPerimeter() const
{
	double perimeter = this->_width *  MULTIPLY_BY_TWO + this->_length * MULTIPLY_BY_TWO;
	return perimeter;
}

void myShapes::Rectangle::move(const Point & other)
{
	int i = 0;
	
	for (i = 0; i < this->_points.size(); i++)
	{
		this->_points[i] = this->_points[i] + other;
	}
}

void myShapes::Rectangle::printDetails() const
{
	cout << "%s", this->getName();
	cout << "       ";
	cout << "%s", this->getType();
	cout << "    ";
	cout << this->getArea();
	cout << "     ";
	cout << this->getPerimeter();
}

void myShapes::Rectangle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char WHITE[] = { 255, 255, 255 };
	board.draw_rectangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(), WHITE, 100.0f).display(disp);
}

void myShapes::Rectangle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0};
	board.draw_rectangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(), BLACK, 100.0f).display(disp);
}


