#pragma once
#include "Polygon.h"

#define POINT_A 0
#define POINT_B 1
#define POINT_C 2
#define DIVISION_BY_FOUR 4

class Triangle : public Polygon
{
public:
	Triangle(const Point& a, const Point& b, const Point& c, const string& type, const string& name);
	virtual ~Triangle();
	
	virtual double getArea() const;
	virtual double getPerimeter() const;
	virtual void draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board);
	virtual void clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board);
	virtual void move(const Point& other); // add the Point to all the points of shape
	void printDetails() const;

	// override functions if need (virtual + pure virtual)
};